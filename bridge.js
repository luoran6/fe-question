function setupWebViewJavascriptBridge (callback) {
  if (window.WebViewJavascriptBridge) { 
    return callback(window.WebViewJavascriptBridge); 
  }
  if (window.WVJBCallbacks) { return window.WVJBCallbacks.push(callback); }
  window.WVJBCallbacks = [callback];
  let WVJBIframe = document.createElement('iframe');
  WVJBIframe.style.display = 'none';
  WVJBIframe.src = 'https://__bridge_loaded__';
  document.documentElement.appendChild(WVJBIframe);
  setTimeout(function () { document.documentElement.removeChild(WVJBIframe) }, 0)
}
function bridgeCall(name,data,callback){
  if (window.bimaBridge && window.bimaBridge.invoke) {
    window.bimaBridge && window.bimaBridge.invoke(name, data, callback)
  } else { 
    setupWebViewJavascriptBridge(bridge => bridge.callHandler(name, data, callback))
  }
}
