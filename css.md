## flex 

1. flex-grow,flex-shrink,flex-basic

* flex-grow:剩余空间分配
```
contianer:width:400px;display:flex;
box1:width:100px;flex-grow:1;
box2:width:200px;flex-grow:2;

ps. box1实际宽度 100px+ 1*100/(1+2) = 133.33px
ps. box2实际宽度 200px+ 2*100/(1+2) = 266.66px

```
* flex-basic:容器预留空间，优先级大于width（auto除外）
```
contianer:width:400px;display:flex;
box1:width:100px;flex-basic:150px;
box2:width:100px;flex-basic:auto;
ps. box1实际宽度150px;
ps. box2实际宽度100px;
```
* flex-shrink:多余空间吸收
```
contianer:width:400px;display:flex;
box1:width:150px;
box2:width:150px;flex-shrink:1;
box3:width:150px;flex-shrink:2;

超出空间 50px，基数：150px*1+150px*1+150px*2 = 600px;
box1 占比 150px*1/600px = 0.25; 吸收50*0.25 = 12.5;  实际宽度 150-12.5 = 137.5px;
box2 占比 150px*1/600px = 0.25; 吸收50*0.25 = 12.5;  实际宽度 150-12.5 = 137.5px;
box3 占比 150px*2/600px = 0.5;  吸收50*0.5 = 25;  实际宽度 150-25 = 125px;
```

2. 分栏高度一致
```
1. position absolute
2. flex布局的中align-items：stretch
```
3. padding , margin百分比
```
  基于父容器的宽，此处的宽不包括padding,border，而是实际content的宽
```
