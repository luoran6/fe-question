## JS 
* 基本数据类型(6种)和Object，基本数据类型包括undefined,null,boolean,number,string,symbol;
* typeof 
``` 
  typeof null //Object，其他都会返回正确的类型
  Object.prototype.toString.call(obj)
```
* []==![] 的计算过程
```
  第一步 ![] = false
  第二步 false = ToNumber(false)=0
  第三步 ToPrimitive([]) = '';
  第四步 Boolean('') = false;
  第五步 ToNumber(false) = 0;
```
## 性能优化
* DNS预解析，预加载，预渲染
```
  <link rel='dns-prefetch' href='//noshz.cn'>
  preload
  prerender
```
* 缓存（强缓存200，协商缓存304）
* 域名分散，cdn（单个域名并发请求上限）
* http2.0 域名收敛
* 图片懒加载
* 减少DOM操作，js放底部
* 图片优化，图片合并，文件压缩合并
* gzip压缩

## HTTP

## Webpack 

## Hybrid

## 算法

## 网络安全

## 项目遇到的问题或亮点技术
1.项目中的性能优化

2.组件上传

3.文件地图标注 pdf.js

4.editor错乱

5.内容同步

6.ux
