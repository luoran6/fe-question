function MyPromise(callback){
  this.value = undefined;
  this.state = 'PENDING';
  this.resolve = function(value){
    this.value = value;
    this.state = 'FULLFILLED';
  }
  this.reject = function(value){
    this.value = value;
    this.state = 'REJECTED';
  }
  callback(this.resolve.bind(this),this.reject.bind(this));
}