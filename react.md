## React
* React keys应确保在同一列表下的key唯一，即兄弟节点之间唯一,不用全局唯一，最好不要用index来表示key


#### React 事件机制
* react事件全部绑定在document上，通过事件冒泡到顶层执行
* e.stopPropagation不能阻止原生事件冒泡，其中的e是react封装过的event事件
* e.nativeEvent.stopPropagation，回调无法执行;因为冒泡是从里到外，执行了原生的阻止冒泡，document当然捕捉不到

#### React createPortal

* portals提供了一种很好的将子节点渲染到父组件意外的DOM节点的方式 
```
ReactDOM.createPortal(child,container)
```
* 适用场景
```
对于 portal 的一个典型用例是当父组件有 overflow: hidden 或 z-index 式，但你需要子组件能够在视觉上“跳出（break out）”其容器。例如，对话框、hovercards以及提示框
```

#### React 高阶组件（HOC）
