## 电话面试第一轮

1. 垂直居中
2. flex布局，主轴交叉轴

```
有flex-direction控制
direction为row:justfy-content为主轴，align-item则为交叉轴
direction为column:justfy-content为交叉轴，align-item则为主轴
```

3. flex可变容器布局
4. rem如何理解

5. 如何实现移动端web 1像素

在移动端存在dpr=2，或dpr=3的情况，需根据media query来判断屏幕属性

```
  @media all and (-webkit-min-device-pixel-ratio: 2) 
  @media all and (-webkit-min-device-pixel-ratio: 3) 
```
  以dpr=2为例，实现移动端1像素的方法有

```
  1.height:1px; transform:scaleY(0.5);   //垂直缩放0.5倍
  2.height:1px; background: linear-gradient(0, #fff, #000); //垂直渐变
  3.box-shadow: 0 0.5px 0 0 #000;
  4.svg
  5.viewport init-scale //判断屏幕dpr实现在移动端的缩放
```
6. viewport如何理解
7. promise如何实现
8. generator如何实现
```
function generator(cb) {
  return (function() {
    var object = {
      next: 0,
      stop: function() {}
    };

    return {
      next: function() {
        var ret = cb(object);
        if (ret === undefined) return { value: undefined, done: true };
        return {
          value: ret,
          done: false
        };
      }
    };
  })();
}
```
9. 深拷贝如何实现(json.stringify,递归调用)
10. 深拷贝数据结构复杂会出现栈溢出，如何解决
```
  异常处理？try catch
```
11. react key的作用

12. cookie如何理解
13. xss和csrf 如何理解
14. tab之间通信，
15. 多个tab淘宝页，其中购物车添加，如何同步其他tab(localstorage)，localstorage有安全问题，怎么解决
16. 模块a依赖模块b，模块b依赖模块a，怎么解决模块间的循环依赖
  
  * 最常见的两种模块格式CommonJS和ES6
  1. CommonJS 通过require引入的模块会将export的执行结果存入缓存中，当再次执行require加载时，不会执行，而是直接拿缓存的结果
```
  // a.js
  exports.done = false;
  var b = require('./b.js');
  console.log('在 a.js 之中，b.done = %j', b.done);
  exports.done = true;
  console.log('a.js 执行完毕');

  //b.js
  exports.done = false;
  var a = require('./a.js');
  console.log('在 b.js 之中，a.done = %j', a.done);
  exports.done = true;
  console.log('b.js 执行完毕');

  //main.js
  var a = require('./a.js');
  var b = require('./b.js');
  console.log('在 main.js 之中, a.done=%j, b.done=%j', a.done, b.done);

  //执行main.js的结果
  $ node main.js
  在 b.js 之中，a.done = false
  b.js 执行完毕
  在 a.js 之中，b.done = true
  a.js 执行完毕
  在 main.js 之中, a.done=true, b.done=true
```
  2. ES6模块的运行机制与CommonJS不一样，它遇到模块加载命令import时，不会去执行模块，而是只生成一个引用。等到真的需要用到时，再到模块里面去取值    
```
  // m1.js
  export var foo = 'bar';
  setTimeout(() => foo = 'baz', 500);

  // m2.js
  import {foo} from './m1.js';
  console.log(foo);
  setTimeout(() => console.log(foo), 500);
```
17. 防抖和节流
18. 数字金额以逗号分隔的形式展现，如何实现
19. native 与 js 通信方式
